from asyncore import loop
from concurrent.futures import thread
from struct import pack
from termios import FF1
import threading
import PIL
from grpc import protos_and_services
import imageio
from fileinput import filename
from itertools import dropwhile
from tkinter import W, Frame, Image, Label, OptionMenu, StringVar, Tk, Canvas, Entry, Text, Button, PhotoImage, filedialog
from cProfile import label
from pathlib import Path
from tokenize import Ignore
from turtle import bgcolor, color, delay, heading, width
from PIL import ImageTk, Image
from matplotlib import image
from matplotlib.pyplot import fill, text
from numpy import outer, size
from tkvideo import tkvideo
import numpy as np
import cv2
import time
import socket

OUTPUT_PATH = Path(__file__).parent
ASSETS_PATH = OUTPUT_PATH / Path("./assets")


def relative_to_assets(path: str) -> Path:
    return ASSETS_PATH / Path(path)

def browse_button():
    global folder_path
    filename=filedialog.askdirectory()
    folder_path.set(filename)
    print(filename)

window = Tk()
window.title('GUI Drone Data Analytics')
# window.geometry("1080x720")
window.attributes('-fullscreen',True)
# window.state('zoomed')
window.configure(bg = "#EAEAEA")


canvas = Canvas(
    window,
    bg = "#EAEAEA",
    height = 982,
    width = 1512,
    bd = 0,
    highlightthickness = 0,
    relief = "ridge"
)

canvas.place(x = 0, y = 0)
button_image_1 = PhotoImage(
    file=relative_to_assets("button_1.png"))
button_1 = Button(
    image=button_image_1,
    borderwidth=0,
    highlightthickness=0,
    command=lambda: print("Plese wait, connecting to drones..."),
    relief="flat"
)
button_1.place(
    x=665.0,
    y=840.0,
    width=118.0,
    height=34
)

canvas.create_rectangle(
    198.0,
    72.0,
    198.0,
    877.0,
    fill="#000000",
    outline="")

canvas.create_rectangle(
    1230.0,
    72.0,
    1230.0000381469727,
    877.0,
    fill="#000000",
    outline="")

canvas.create_text(
    22.0,
    97.0,
    anchor="nw",
    text="Flight Configuration: ",
    fill="#000000",
    font=("Inter ExtraBold", 14 * -1)
)

canvas.create_text(
    1250.0,
    100.0,
    anchor="nw",
    text="Flight Data: ",
    fill="#000000",
    font=("Inter ExtraBold", 14 * -1)
)

canvas.create_rectangle(
    22.0,
    118.0,
    178.0,
    118.0,
    fill="#000000",
    outline="")

canvas.create_rectangle(
    1250.0,
    123.0,
    1400.0,
    123.0,
    fill="#000000",
    outline="")

canvas.create_text(
    1250.0,
    158.0,
    anchor="nw",
    text="Long     : X.XXXXXXXX​\n\nLat        : Y.YYYYYYYYY​\n\nAlt        : XXX.XX m​\n\nHead    : XXX.XX˚​\n\nHFoV    : XXX.XX˚​\n\nVFoV    : XXX.XX˚​\n\nSpeed: XXX.XX​",
    fill="#000000",
    font=("Inter Medium", 14 * -1)
)

canvas.create_text(
    24.0,
    144.0,
    anchor="nw",
    text="Drone source",
    fill="#000000",
    font=("Inter SemiBold", 12 * -1)
)

canvas.create_rectangle(
    24.0,
    165.0,
    178.0,
    185.0,
    fill="#FFFFFF",
    outline="")

canvas.create_rectangle(
    158.0,
    165.0,
    178.0,
    185.0,
    fill="#7F7F7F",
    outline="")

canvas.create_text(
    160.0,
    168.0,
    anchor="nw",
    text="...",
    fill="#000000",
    font=("Inter SemiBold", 10 * -1)
)

# Menu Drones Version Save To
folder_path=StringVar()
saveto=Label(master=window, textvariable=folder_path, bg="#ffffff", borderwidth=2)
saveto.place(x=24, y=165, width=150, height=30)
textsaveto=Button(text="...", command=browse_button, bg="#ffffff")
textsaveto.place(x=140, y=165, height=30)

canvas.create_text(
    23.0,
    205.0,
    anchor="nw",
    text="Analytics method",
    fill="#000000",
    font=("Inter SemiBold", 12 * -1)
)

# Menu Analytic Methode version Save To
folder_path=StringVar()
saveto=Label(master=window, textvariable=folder_path, bg="#ffffff", borderwidth=2)
saveto.place(x=24, y=226, width=150, height=30)
textsaveto=Button(text="...", command=browse_button, bg="#ffffff")
textsaveto.place(x=140, y=226, height=30)

canvas.create_text(
    24.0,
    266.0,
    anchor="nw",
    text="Save to",
    fill="#000000",
    font=("Inter SemiBold", 12 * -1)
)

#SAVE TO
folder_path=StringVar()
saveto=Label(master=window, textvariable=folder_path, bg="#ffffff", borderwidth=2)
saveto.place(x=25, y=287, width=150, height=30)
textsaveto=Button(text="...", command=browse_button, bg="#ffffff")
textsaveto.place(x=140, y=287, height=30)

# Video not using thread
# video_label = Label(window)
# video_label.place(
#     x=220.0,
#     y=75.0,
#     width=983,
#     height=737
# )
# player = tkvideo("assets/video.mp4", video_label,
#                  loop = 1, size=(983,737))

# Video with Threads
video_name="assets/video.mp4"
prev_frame_time=0
new_frame_time=0
video = imageio.get_reader(video_name)

def display_video(label):
    for image in video.iter_data():
        image=video.get_next_data()
        img = Image.fromarray(image)
        image_frame=ImageTk.PhotoImage(image=img)
        label.config(image=image_frame)
        label.image = image_frame

video_label = Label(window)
video_label.pack()
video_label.place(
    x=217.0,
    y=75.0,
    width=991,
    height=743,
)
thread = threading.Thread(target=display_video, args=(video_label,))


window.resizable(False, False)
thread.start()
# player.play()
window.mainloop()
