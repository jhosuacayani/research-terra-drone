# Dataset-Kendaraan



## Getting started

Training Dataset Kendaraan - motor, mobil, dan truk
----
Hai, didalam sini terdapat Dataset dengan object mobil, motor, dan truk yang diambil dari atas menggunakan drone dengan jenis file jpeg, xml, dan csv, bahkan sudah terbagi menjadi train_labels dan test_labels beserta recordnya, sehingga tinggal dilakukan training menggunakan Arsitektur yang tersedia seperti SSD_Mobilenet_V1 ataupun V2.

Terdapat juga File Google Collab dari TECHZIZOU untuk memudahkan kalian dalam memahami code trainingnya karena sudah terdapat penjelasannya, pastikan file *.config diatur terlebih dahulu PATH nya sesuaikan dengan train.record, train_labels.csv, test_labels.csv, test.record, dan label_map.pbtxt


----
Hi, here is a dataset with objects of cars, motorcycles, and trucks taken from above using a drone with jpeg, xml, and csv file types, even divided into train_labels and test_labels along with their records, so all that remains is training using available architectures such as SSD_Mobilenet_V1 or V2. There is also a Google collab from TECHZIZOU to make it easier for you to understand the code.

There is also a Google Collab File from TECHZIZOU to make it easier for you to understand the training code because there is an explanation, make sure the *.config file is first set PATH to match train.record, train_labels.csv, test_labels.csv, test.record, and label_map.pbtxt
